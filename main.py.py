#!/usr/bin/python

import pandas
import jaydebeapi
import time
import datetime
import os
now = datetime.datetime.now()
date_time=now.strftime("%d.%m.%Y %H:%M:%S")

import pandas
import jaydebeapi
conn = jaydebeapi.connect(
'oracle.jdbc.driver.OracleDriver',
'jdbc:oracle:thin:demipt2/peregrintook@de-oracle.chronosavant.ru:1521/deoracle',
['demipt2','peregrintook'],
'/home/demipt2/ojdbc8.jar'
)
conn.jconn.setAutoCommit(False)
curs = conn.cursor()


print('подключение выполнено, старт загрузки первого дня')

###Загружаем первый день Blacklist###

#Очищаем стэйджинг таблицы терминалов
curs.execute("delete from demipt2.sape_stg_passport_blacklist")
#Зачитываем файл
df = pandas.read_excel( '/home/demipt2/sape/passport_blacklist_01032021.xlsx', sheet_name='blacklist',header=0, decimal=',', index_col=None )
df['date']=df['date'].astype(str)
#Отправляем данные в базу
curs.executemany( "insert into demipt2.sape_stg_passport_blacklist (ENTRY_DT, PASSPORT_NUM) values (to_date(?, 'YYYY-MM-DD'), ?)",
df.values.tolist() )

conn.commit()
print('10%')


#Делаем простую вставку изменений
curs.execute("""insert into demipt2.sape_dwh_fact_pssprt_blcklst (PASSPORT_NUM, ENTRY_DT, 	VERSION_DT )
select PASSPORT_NUM, ENTRY_DT, (select max(entry_dt) from demipt2.sape_stg_passport_blacklist) from demipt2.sape_stg_passport_blacklist""");


conn.commit();
os.rename("/home/demipt2/sape/passport_blacklist_01032021.xlsx", "/home/demipt2/sape/archive/passport_blacklist_01032021.xlsx.backup")
###Загружаем транзакции####

#Очищаем стэйджинг таблицы терминалов
curs.execute("delete from demipt2.sape_stg_transactions")
#Зачитываем файл
df = pandas.read_csv( '/home/demipt2/sape/transactions_01032021.txt', sep=';', header=0, decimal=',', index_col=None )
df['transaction_date']=df['transaction_date'].astype(str)
#Отправляем данные в базу
curs.executemany( "insert into demipt2.sape_stg_transactions (TRANS_ID, TRANS_DATE, AMT, CARD_NUM, OPER_TYPE, OPER_RESULT, TERMINAL) values (?, to_date(?, 'yyyy-mm-dd hh24:mi:ss'), ?, ?, ?, ?, ?)",
df.values.tolist() )



curs.execute("""insert into demipt2.sape_dwh_fact_transactions (TRANS_ID, TRANS_DATE, CARD_NUM, OPER_TYPE, AMT, OPER_RESULT, TERMINAL )
select 
    TRANS_ID,
    TRANS_DATE,
    CARD_NUM,
    OPER_TYPE,
    AMT,
    OPER_RESULT,
    TERMINAL 
from demipt2.sape_stg_transactions""");

conn.commit()

os.rename("/home/demipt2/sape/transactions_01032021.txt", "/home/demipt2/sape/archive/transactions_01032021.txt.backup")

print('30%')
###Загружаем первый день terminals###

#Очищаем стэйджинг таблицы терминалов
curs.execute("delete from demipt2.sape_stg_term_part1")
curs.execute("delete from demipt2.sape_stg_terminals_del")
#Зачитываем файл
df = pandas.read_excel( '/home/demipt2/sape/terminals_01032021.xlsx', sheet_name='terminals',header=0, decimal=',', index_col=None )

#Отправляем данные в базу
curs.executemany( "insert into demipt2.sape_stg_term_part1 (terminal_id, terminal_type, terminal_city, terminal_address,download_dt) values (?,?,?,?, to_date( '2021-03-01', 'YYYY-MM-DD' ))",
df.values.tolist() )

conn.commit()

curs.execute("""insert into demipt2.sape_stg_terminals_del ( terminal_id )
select 
    terminal_id 
from demipt2.sape_stg_term_part1""")
conn.commit()

time.sleep(1)

curs.execute("""merge into demipt2.sape_dwh_dim_terminals_hist tgt
using (
    select 
        s.terminal_id, 
        download_dt
    from demipt2.sape_stg_term_part1 s
    left join demipt2.sape_dwh_dim_terminals_hist t
    on s.terminal_id = t.terminal_id and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
    where 
        t.terminal_id is not null and ( 1=0
 or t.TERMINAL_TYPE <> s.TERMINAL_TYPE or (t.TERMINAL_TYPE is null and s.TERMINAL_TYPE is not null) or ( t.TERMINAL_TYPE is not null and s.TERMINAL_TYPE is null)
 or t.TERMINAL_CITY <> s.TERMINAL_CITY or (t.TERMINAL_CITY is null and s.TERMINAL_CITY is not null) or ( t.TERMINAL_CITY is not null and s.TERMINAL_CITY is null)
 or t.TERMINAL_ADDRESS <> s.TERMINAL_ADDRESS or (t.TERMINAL_ADDRESS is null and s.TERMINAL_ADDRESS is not null) or ( t.TERMINAL_ADDRESS is not null and s.TERMINAL_ADDRESS is null))
    ) stg
on ( tgt.terminal_id = stg.terminal_id )
when matched then update set effective_to = download_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )""");

conn.commit()
time.sleep(1)

curs.execute("""insert into demipt2.sape_dwh_dim_terminals_hist ( terminal_id, terminal_type, terminal_city, terminal_address, effective_from, effective_to, deleted_flg )
select 
    t.terminal_id, 
    t. terminal_type, 
    t.terminal_city, 
    t.terminal_address,
    download_dt as effective_from,
    to_date( '9999-12-31', 'YYYY-MM-DD') as effective_to, 
    'N' as deleted_flg
from demipt2.sape_stg_term_part1 t 
left join demipt2.sape_dwh_dim_terminals_hist s on t.terminal_id=s.terminal_id and effective_to=to_date( '9999-12-31', 'YYYY-MM-DD')
where s.terminal_id is null or ( 1=0
 or t.TERMINAL_TYPE <> s.TERMINAL_TYPE or (t.TERMINAL_TYPE is null and s.TERMINAL_TYPE is not null) or ( t.TERMINAL_TYPE is not null and s.TERMINAL_TYPE is null)
 or t.TERMINAL_CITY <> s.TERMINAL_CITY or (t.TERMINAL_CITY is null and s.TERMINAL_CITY is not null) or ( t.TERMINAL_CITY is not null and s.TERMINAL_CITY is null)
 or t.TERMINAL_ADDRESS <> s.TERMINAL_ADDRESS or (t.TERMINAL_ADDRESS is null and s.TERMINAL_ADDRESS is not null) or ( t.TERMINAL_ADDRESS is not null and s.TERMINAL_ADDRESS is null))""")
         
conn.commit()


#удаления
curs.execute("""insert into demipt2.sape_dwh_dim_terminals_hist ( terminal_id, terminal_type, terminal_city, terminal_address, effective_from, effective_to, deleted_flg  )
select 
    terminal_id, 
	terminal_type, 
    terminal_city, 
    terminal_address,
	to_date( '2021-03-01', 'YYYY-MM-DD' ) as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'Y' as deleted_flg
from (
    select 
       t.terminal_id, 
        terminal_type, 
        terminal_city, 
        terminal_address
    from demipt2.sape_dwh_dim_terminals_hist t
    left join demipt2.sape_stg_terminals_del s
    on t.terminal_id = s.terminal_id 
    where s.terminal_id is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N' )""")
 
          
conn.commit()
    
curs.execute("""update demipt2.sape_dwh_dim_terminals_hist
set effective_to = to_date( '2021-03-01', 'YYYY-MM-DD' ) - interval '1' second
where terminal_id in (
    select
       t.terminal_id
    from demipt2.sape_dwh_dim_terminals_hist t
    left join demipt2.sape_stg_terminals_del s
    on t.terminal_id = s.terminal_id  
    where s.terminal_id is null and deleted_flg='N' and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ))
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'""");

conn.commit()
os.rename("/home/demipt2/sape/terminals_01032021.xlsx", "/home/demipt2/sape/archive/terminals_01032021.xlsx.backup")

print('50%')

###Загружаем изменения по таблице аккаунты###


curs.execute("delete from demipt2.sape_stg_accounts");

conn.commit()

curs.execute("delete from demipt2.sape_stg_accounts_del");

time.sleep(2)
conn.commit()

curs.execute("""insert into demipt2.sape_stg_accounts ( account_num, valid_to, client, create_dt, update_dt )
select 
    account, 
    valid_to, 
    client, 
    create_dt, 
    update_dt
from bank.accounts
where create_dt > ( 
    select 
        coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'accounts' ) or 
    update_dt>( 
    select 
        coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'accounts' )""");
 
 
 
time.sleep(2)
  
conn.commit()
   
curs.execute("""insert into demipt2.sape_stg_accounts_del ( account_num )
select 
    account 
from bank.accounts""");

time.sleep(2)
conn.commit()

curs.execute("""merge into demipt2.sape_dwh_dim_accounts_hist tgt
using (
    select s.account_num,
           s.update_dt
    from demipt2.sape_stg_accounts s
    left join demipt2.sape_dwh_dim_accounts_hist t
    on s.account_num = t.account_num and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
    where 
        t.account_num is not null and ( 1=0
  or t.VALID_TO <> s.VALID_TO or (t.VALID_TO is null and s.VALID_TO is not null) or ( t.VALID_TO is not null and s.VALID_TO is null)
 or t.CLIENT <> s.CLIENT or (t.CLIENT is null and s.CLIENT is not null) or ( t.CLIENT is not null and s.CLIENT is null))
    ) stg
on ( tgt.account_num = stg.account_num )
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )""")

time.sleep(2)
conn.commit()

curs.execute("""insert into demipt2.sape_dwh_dim_accounts_hist ( account_num, valid_to, client, effective_from, effective_to, deleted_flg )
select 
	account_num, 
	valid_to,
    client,
	coalesce (update_dt,create_dt) as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'N' as deleted_flg
from demipt2.sape_stg_accounts""");

time.sleep(2)
conn.commit()

#удаления

curs.execute("""insert into demipt2.sape_dwh_dim_accounts_hist ( account_num, valid_to, client, effective_from, effective_to, deleted_flg  )
select 
    account_num, 
	valid_to,
    client,
	to_date(?, 'dd.mm.YYYY hh24:mi:ss') as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'Y' as deleted_flg
from (
    select
        t.account_num, 
        valid_to,
        client
    from demipt2.sape_dwh_dim_accounts_hist t
    left join demipt2.sape_stg_accounts_del s
    on t.account_num = s.account_num 
    where s.account_num is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N' )""", (date_time,))

time.sleep(2)
conn.commit()

   
curs.execute ("""update demipt2.sape_dwh_dim_accounts_hist
set effective_to = to_date(?, 'dd.mm.YYYY hh24:mi:ss') - interval '1' second
where account_num in (
    select
       t.account_num
    from demipt2.sape_dwh_dim_accounts_hist t
    left join demipt2.sape_stg_accounts_del s
    on t.account_num = s.account_num  
    where s.account_num is null and deleted_flg='N' and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ))
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'""", (date_time,));

time.sleep(2)
conn.commit()

#Обновляем метаданные.
curs.execute("""update demipt2.sape_meta_trans
set last_update_dt = ( Coalesce((select max(update_dt) from demipt2.sape_stg_accounts), (select max(create_dt) from demipt2.sape_stg_accounts ) ))
where table_db = 'DEMIPT2' and table_name = 'accounts'""");

conn.commit()
time.sleep(1)

print('70%')

###Загружаем карты###

curs.execute("delete from demipt2.sape_stg_cards");

conn.commit()

curs.execute("delete from demipt2.sape_stg_cards_del");

conn.commit()

curs.execute("""insert into demipt2.sape_stg_cards ( card_num, account_num, create_dt, update_dt )
select 
    trim(card_num), 
    account, 
    create_dt, 
    update_dt 
from bank.cards
where create_dt > ( 
    select coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'cards' ) or update_dt>( 
    select coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'cards' )""");

time.sleep(2)  
conn.commit()

curs.execute ("""insert into demipt2.sape_stg_cards_del ( card_num )
select 
    trim(card_num) 
from bank.cards""");

time.sleep(1)
conn.commit()

curs.execute ("""merge into demipt2.sape_dwh_dim_cards_hist tgt
using (
    select s.card_num,
           s.update_dt
    from demipt2.sape_stg_cards s
    left join demipt2.sape_dwh_dim_cards_hist t
    on s.card_num = t.card_num and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
    where 
        t.card_num is not null and ( 1=0
  or t.account_num <> s.account_num or (t.account_num is null and s.account_num is not null) or ( t.account_num is not null and s.account_num is null))
    ) stg
on ( tgt.card_num = stg.card_num )
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )""")

time.sleep(1)
conn.commit()

curs.execute ("""insert into demipt2.sape_dwh_dim_cards_hist ( card_num, account_num, effective_from, effective_to, deleted_flg )
select 
	card_num, 
	account_num,
	coalesce (update_dt, create_dt) as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'N' as deleted_flg
from demipt2.sape_stg_cards""")

conn.commit()
time.sleep(1)
#Удаления
curs.execute ("""insert into demipt2.sape_dwh_dim_cards_hist (card_num, account_num, effective_from, effective_to, deleted_flg)
select 
    card_num, 
	account_num,
	to_date(?, 'dd.mm.YYYY hh24:mi:ss') as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'Y' as deleted_flg
from (
    select 
       t.card_num, 
       account_num
    from demipt2.sape_dwh_dim_cards_hist t
    left join demipt2.sape_stg_cards_del s
    on t.card_num = s.card_num 
    where s.card_num is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N' )""", (date_time,))
    
conn.commit()
time.sleep(1)	
curs.execute("""update demipt2.sape_dwh_dim_cards_hist
set effective_to = to_date(?, 'dd.mm.YYYY hh24:mi:ss') - interval '1' second
where card_num in (
    select
       t.card_num
    from demipt2.sape_dwh_dim_cards_hist t
    left join demipt2.sape_stg_cards_del s
    on t.card_num = s.card_num  
    where s.card_num is null and deleted_flg='N' and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ))
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'""", (date_time,))

conn.commit()
time.sleep(1)
#Обновляем метаданные.
curs.execute("""update demipt2.sape_meta_trans
set last_update_dt = ( Coalesce((select max(update_dt) from demipt2.sape_stg_cards), (select max(create_dt) from demipt2.sape_stg_cards ) ))
where table_db = 'DEMIPT2' and table_name = 'cards'""")

conn.commit()
time.sleep(1)

print('90%')

###Загружаем таблицу клиенты###

curs.execute("delete from demipt2.sape_stg_clients")

conn.commit()

curs.execute("delete from demipt2.sape_stg_clients_del")

conn.commit()

curs.execute("""insert into demipt2.sape_stg_clients ( client_id, last_name, first_name, patronymic, date_of_birth, passport_num, passport_valid_to, phone, create_dt, update_dt )
select 
    client_id, 
    last_name, 
    first_name, 
    patronymic, 
    date_of_birth, 
    passport_num, 
    passport_valid_to, 
    phone, 
    create_dt, 
    update_dt 
from bank.clients
where create_dt > ( 
    select coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'clients' ) or update_dt>( 
    select coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'clients' )""")
    
time.sleep(2)    
conn.commit()
    
curs.execute("""insert into demipt2.sape_stg_clients_del ( client_id )
select 
    client_id 
from bank.clients""")

conn.commit()
time.sleep(1)
curs.execute("""merge into demipt2.sape_dwh_dim_clients_hist tgt
using (
    select s.client_id,
           s.update_dt
    from demipt2.sape_stg_clients s
    left join demipt2.sape_dwh_dim_clients_hist t
    on s.client_id = t.client_id and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
    where 
        t.client_id is not null and ( 1=0
 or t.LAST_NAME <> s.LAST_NAME or (t.LAST_NAME is null and s.LAST_NAME is not null) or ( t.LAST_NAME is not null and s.LAST_NAME is null)
 or t.FIRST_NAME <> s.FIRST_NAME or (t.FIRST_NAME is null and s.FIRST_NAME is not null) or ( t.FIRST_NAME is not null and s.FIRST_NAME is null)
 or t.PATRONYMIC <> s.PATRONYMIC or (t.PATRONYMIC is null and s.PATRONYMIC is not null) or ( t.PATRONYMIC is not null and s.PATRONYMIC is null)
 or t.DATE_OF_BIRTH <> s.DATE_OF_BIRTH or (t.DATE_OF_BIRTH is null and s.DATE_OF_BIRTH is not null) or ( t.DATE_OF_BIRTH is not null and s.DATE_OF_BIRTH is null)
 or t.PASSPORT_NUM <> s.PASSPORT_NUM or (t.PASSPORT_NUM is null and s.PASSPORT_NUM is not null) or ( t.PASSPORT_NUM is not null and s.PASSPORT_NUM is null)
 or t.PASSPORT_VALID_TO <> s.PASSPORT_VALID_TO or (t.PASSPORT_VALID_TO is null and s.PASSPORT_VALID_TO is not null) or ( t.PASSPORT_VALID_TO is not null and s.PASSPORT_VALID_TO is null)
 or t.PHONE <> s.PHONE or (t.PHONE is null and s.PHONE is not null) or ( t.PHONE is not null and s.PHONE is null))
    ) stg
on ( tgt.client_id = stg.client_id )
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )""")

conn.commit()
time.sleep(1)
curs.execute("""insert into demipt2.sape_dwh_dim_clients_hist ( CLIENT_ID, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_OF_BIRTH, PASSPORT_VALID_TO, PASSPORT_NUM, PHONE, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG )
select 
	client_id, 
	last_name,
    first_name,
    patronymic,
    date_of_birth,
    passport_valid_to,
    passport_num,
    phone,
	coalesce (update_dt,create_dt) as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'N' as deleted_flg
from demipt2.sape_stg_clients""")

conn.commit()
time.sleep(1)

#Удаления

curs.execute("""insert into demipt2.sape_dwh_dim_clients_hist ( CLIENT_ID, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_OF_BIRTH, PASSPORT_VALID_TO, PASSPORT_NUM, PHONE, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG )
select 
    client_id, 
	last_name,
    first_name,
    patronymic,
    date_of_birth,
    passport_valid_to,
    passport_num,
    phone,
	to_date(?, 'dd.mm.YYYY hh24:mi:ss') as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'Y' as deleted_flg
from (
    select
      t.client_id, 
      t.last_name,
      t.first_name,
      t.patronymic,
      t.date_of_birth,
      t.passport_valid_to,
      t.passport_num,
      t.phone
    from demipt2.sape_dwh_dim_clients_hist t
    left join demipt2.sape_stg_clients_del s
    on t.client_id = s.client_id 
    where s.client_id is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N' )""", (date_time,))
	
conn.commit()	
time.sleep(1)
    
curs.execute("""update demipt2.sape_dwh_dim_clients_hist
set effective_to = to_date(?, 'dd.mm.YYYY hh24:mi:ss') - interval '1' second
where client_id in (
    select
       t.client_id
    from demipt2.sape_dwh_dim_clients_hist t
    left join demipt2.sape_stg_clients_del s
    on t.client_id = s.client_id  
    where s.client_id is null and deleted_flg='N' and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ))
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'""", (date_time,))

conn.commit()
time.sleep(1)

#Обновляем метаданные

curs.execute("""update demipt2.sape_meta_trans
set last_update_dt = ( Coalesce ( (select max(update_dt) from demipt2.sape_stg_clients), (select max(create_dt) from demipt2.sape_stg_clients )))
where table_db = 'DEMIPT2' and table_name = 'clients'""")

print('Данные загружены, идет формирование ежедневного отчета')

###Построение отчета###
conn.commit()
time.sleep(1)
curs.execute("""insert into sape_rep_fraud (event_dt, passport, fio, phone, event_type, report_dt) 
select  distinct
    trans_date, 
    passport_num, 
    fio, 
    phone, 
    prichina, 
    current_date
from (
    select 
        trans_id,
        case 
            when lead(terminal_city) over(partition by t.card_num order by trans_date)<>terminal_city  and (lead(trans_date) over(partition by t.card_num order by trans_date)-trans_date)<1 then lead(trans_date) over(partition by t.card_num order by trans_date)
            else trans_date
        end trans_date,    
        t.card_num, 
        c.account_num, 
        valid_to, 
        client, 
        oper_type, 
        amt, 
        oper_result, 
        terminal, 
        terminal_city, 
        last_name||' '||first_name||' '||patronymic as fio, 
        date_of_birth, 
        cl.passport_num, 
        phone,
        case
            when (coalesce(passport_valid_to, to_date('9999-12-31', 'YYYY-MM-DD' ))<trans_date and cl.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' ) and cl.deleted_flg='N') then '1'
            when (p.passport_num is not null and trunc(trans_date)=trunc(entry_dt) and version_dt=(select max(version_dt) from demipt2.sape_dwh_fact_pssprt_blcklst )) then '1'
            when (valid_to<trans_date and a.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' )and a.deleted_flg='N') then '2'
            when lead(terminal_city) over(partition by t.card_num order by trans_date)<>terminal_city  and (lead(trans_date) over(partition by t.card_num order by trans_date)-trans_date)<1 then '3'
            else 'OKEY'
        end prichina
    from demipt2.sape_dwh_fact_transactions  t
    left join demipt2.sape_dwh_dim_cards_hist c
    on t.card_num=c.card_num and c.effective_to=to_date( '9999-12-31', 'YYYY-MM-DD' )and c.deleted_flg='N'
    left join demipt2.sape_dwh_dim_accounts_hist a
    on c.account_num=a.account_num  and a.effective_to=to_date( '9999-12-31', 'YYYY-MM-DD' )and a.deleted_flg='N'
    left join demipt2.sape_dwh_dim_clients_hist cl
    on client=client_id and cl.effective_to=to_date( '9999-12-31', 'YYYY-MM-DD' )and cl.deleted_flg='N'
    left join demipt2.sape_dwh_fact_pssprt_blcklst p
    on cl.passport_num=p.passport_num
    left join demipt2.sape_dwh_dim_terminals_hist e
    on terminal=terminal_id and e.effective_to=to_date( '9999-12-31', 'YYYY-MM-DD' )and e.deleted_flg='N'
    where trans_date>=to_date ( '28.02.2021 23:00:00','DD.MM.YYYY HH24:MI:SS' ) and trans_date<to_date ( '02.03.2021 00:00:00','DD.MM.YYYY HH24:MI:SS' ))
where prichina<>'OKEY' and trans_date>=to_date ( '01.03.2021', 'dd.mm.yyyy' ) and trans_date<to_date ( '02.03.2021', 'dd.mm.yyyy' )""")

conn.commit()



curs.execute("""insert into sape_rep_fraud (event_dt, passport, fio, phone, event_type, report_dt) 
select 
    min(trans_date), 
    passport_num, 
    fio, phone, 
    '4', 
    current_date  
from (
    select 
        trans_date, 
        passport_num, 
        last_name||' '||first_name||' '||patronymic as fio, 
        phone, 
        sum, 
        oper_result 
    from (
        select 
            trans_date, 
            passport_num, 
            last_name,
            first_name,
            patronymic, 
            oper_type, 
            phone, 
            oper_result,
            sum(q) over (partition by card_num order by trans_date range between interval '20' minute preceding and current row) sum
        from (
            select 
                trans_date, 
                t.card_num, 
                passport_num, 
                oper_type, 
                first_name, 
                last_name, 
                patronymic, 
                phone, 
                oper_result, 
                amt, 
                case
                    when oper_result='REJECT' and amt>lead(amt)over(partition by t.card_num order by trans_date) and oper_type<>'DEPOSIT'  then 1
                    else 0
                end q
            from sape_dwh_fact_transactions t
            left join demipt2.sape_dwh_dim_cards_hist c
            on t.card_num=c.card_num and c.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' )and c.deleted_flg='N'
            left join demipt2.sape_dwh_dim_accounts_hist a
            on c.account_num=a.account_num  and a.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' )and a.deleted_flg='N'
            left join demipt2.sape_dwh_dim_clients_hist cl
            on client=client_id and cl.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' )and cl.deleted_flg='N'))
where sum>=3 AND oper_result='SUCCESS' and trans_date>=to_date ('01.03.2021', 'dd.mm.yyyy') and trans_date<to_date ('02.03.2021', 'dd.mm.yyyy'))
group by passport_num, fio, phone, current_date""")

conn.commit()
print('Отчет подготовлен')
time.sleep(1)

print('старт загрузки второго дня')

###Загружаем первый день Blacklist###

#Очищаем стэйджинг таблицы терминалов
curs.execute("delete from demipt2.sape_stg_passport_blacklist")
#Зачитываем файл
df = pandas.read_excel( '/home/demipt2/sape/passport_blacklist_02032021.xlsx', sheet_name='blacklist',header=0, decimal=',', index_col=None )
df['date']=df['date'].astype(str)
#Отправляем данные в базу
curs.executemany( "insert into demipt2.sape_stg_passport_blacklist (ENTRY_DT, PASSPORT_NUM) values (to_date(?, 'YYYY-MM-DD'), ?)",
df.values.tolist() )

conn.commit()
print('15%')


#Делаем простую вставку изменений
curs.execute("""insert into demipt2.sape_dwh_fact_pssprt_blcklst (PASSPORT_NUM, ENTRY_DT, 	VERSION_DT )
select PASSPORT_NUM, ENTRY_DT, (select max(entry_dt) from demipt2.sape_stg_passport_blacklist) from demipt2.sape_stg_passport_blacklist""");


#Фиксируем транзакцию
conn.commit();

os.rename("/home/demipt2/sape/passport_blacklist_02032021.xlsx", "/home/demipt2/sape/archive/passport_blacklist_02032021.xlsx.backup")
###Загружаем транзакции####

#Очищаем стэйджинг таблицы терминалов
curs.execute("delete from demipt2.sape_stg_transactions")
#Зачитываем файл
df = pandas.read_csv( '/home/demipt2/sape/transactions_02032021.txt', sep=';', header=0, decimal=',', index_col=None )
df['transaction_date']=df['transaction_date'].astype(str)
#Отправляем данные в базу
curs.executemany( "insert into demipt2.sape_stg_transactions (TRANS_ID, TRANS_DATE, AMT, CARD_NUM, OPER_TYPE, OPER_RESULT, TERMINAL) values (?, to_date(?, 'yyyy-mm-dd hh24:mi:ss'), ?, ?, ?, ?, ?)",
df.values.tolist() )



curs.execute("""insert into demipt2.sape_dwh_fact_transactions (TRANS_ID, TRANS_DATE, CARD_NUM, OPER_TYPE, AMT, OPER_RESULT, TERMINAL )
select 
    TRANS_ID,
    TRANS_DATE,
    CARD_NUM,
    OPER_TYPE,
    AMT,
    OPER_RESULT,
    TERMINAL 
from demipt2.sape_stg_transactions""");

conn.commit()

os.rename("/home/demipt2/sape/transactions_02032021.txt", "/home/demipt2/sape/archive/transactions_02032021.txt.backup")
print('30%')


###Загружаем первый день terminals###

#Очищаем стэйджинг таблицы терминалов
curs.execute("delete from demipt2.sape_stg_term_part1")
curs.execute("delete from demipt2.sape_stg_terminals_del")
#Зачитываем файл
df = pandas.read_excel( '/home/demipt2/sape/terminals_02032021.xlsx', sheet_name='terminals',header=0, decimal=',', index_col=None )

#Отправляем данные в базу
curs.executemany( "insert into demipt2.sape_stg_term_part1 (terminal_id, terminal_type, terminal_city, terminal_address,download_dt) values (?,?,?,?, to_date( '2021-03-02', 'YYYY-MM-DD' ))",
df.values.tolist() )

conn.commit()

curs.execute("""insert into demipt2.sape_stg_terminals_del ( terminal_id )
select 
    terminal_id 
from demipt2.sape_stg_term_part1""")
conn.commit()

time.sleep(1)

curs.execute("""merge into demipt2.sape_dwh_dim_terminals_hist tgt
using (
    select 
        s.terminal_id, 
        download_dt
    from demipt2.sape_stg_term_part1 s
    left join demipt2.sape_dwh_dim_terminals_hist t
    on s.terminal_id = t.terminal_id and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
    where 
        t.terminal_id is not null and ( 1=0
 or t.TERMINAL_TYPE <> s.TERMINAL_TYPE or (t.TERMINAL_TYPE is null and s.TERMINAL_TYPE is not null) or ( t.TERMINAL_TYPE is not null and s.TERMINAL_TYPE is null)
 or t.TERMINAL_CITY <> s.TERMINAL_CITY or (t.TERMINAL_CITY is null and s.TERMINAL_CITY is not null) or ( t.TERMINAL_CITY is not null and s.TERMINAL_CITY is null)
 or t.TERMINAL_ADDRESS <> s.TERMINAL_ADDRESS or (t.TERMINAL_ADDRESS is null and s.TERMINAL_ADDRESS is not null) or ( t.TERMINAL_ADDRESS is not null and s.TERMINAL_ADDRESS is null))
    ) stg
on ( tgt.terminal_id = stg.terminal_id )
when matched then update set effective_to = download_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )""");

conn.commit()
time.sleep(1)

curs.execute("""insert into demipt2.sape_dwh_dim_terminals_hist ( terminal_id, terminal_type, terminal_city, terminal_address, effective_from, effective_to, deleted_flg )
select 
    t.terminal_id, 
    t. terminal_type, 
    t.terminal_city, 
    t.terminal_address,
    download_dt as effective_from,
    to_date( '9999-12-31', 'YYYY-MM-DD') as effective_to, 
    'N' as deleted_flg
from demipt2.sape_stg_term_part1 t 
left join demipt2.sape_dwh_dim_terminals_hist s on t.terminal_id=s.terminal_id and effective_to=to_date( '9999-12-31', 'YYYY-MM-DD')
where s.terminal_id is null or ( 1=0
 or t.TERMINAL_TYPE <> s.TERMINAL_TYPE or (t.TERMINAL_TYPE is null and s.TERMINAL_TYPE is not null) or ( t.TERMINAL_TYPE is not null and s.TERMINAL_TYPE is null)
 or t.TERMINAL_CITY <> s.TERMINAL_CITY or (t.TERMINAL_CITY is null and s.TERMINAL_CITY is not null) or ( t.TERMINAL_CITY is not null and s.TERMINAL_CITY is null)
 or t.TERMINAL_ADDRESS <> s.TERMINAL_ADDRESS or (t.TERMINAL_ADDRESS is null and s.TERMINAL_ADDRESS is not null) or ( t.TERMINAL_ADDRESS is not null and s.TERMINAL_ADDRESS is null))""")
         
conn.commit()


#удаления
curs.execute("""insert into demipt2.sape_dwh_dim_terminals_hist ( terminal_id, terminal_type, terminal_city, terminal_address, effective_from, effective_to, deleted_flg  )
select 
    terminal_id, 
	terminal_type, 
    terminal_city, 
    terminal_address,
	to_date( '2021-03-02', 'YYYY-MM-DD' ) as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'Y' as deleted_flg
from (
    select 
       t.terminal_id, 
        terminal_type, 
        terminal_city, 
        terminal_address
    from demipt2.sape_dwh_dim_terminals_hist t
    left join demipt2.sape_stg_terminals_del s
    on t.terminal_id = s.terminal_id 
    where s.terminal_id is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N' )""")
 
          
conn.commit()
    
curs.execute("""update demipt2.sape_dwh_dim_terminals_hist
set effective_to = to_date( '2021-03-02', 'YYYY-MM-DD' ) - interval '1' second
where terminal_id in (
    select
       t.terminal_id
    from demipt2.sape_dwh_dim_terminals_hist t
    left join demipt2.sape_stg_terminals_del s
    on t.terminal_id = s.terminal_id  
    where s.terminal_id is null and deleted_flg='N' and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ))
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'""");

conn.commit()
os.rename("/home/demipt2/sape/terminals_02032021.xlsx", "/home/demipt2/sape/archive/terminals_02032021.xlsx.backup")

print('50%')

###Загружаем изменения по таблице аккаунты###


curs.execute("delete from demipt2.sape_stg_accounts");

conn.commit()

curs.execute("delete from demipt2.sape_stg_accounts_del");

time.sleep(2)
conn.commit()

curs.execute("""insert into demipt2.sape_stg_accounts ( account_num, valid_to, client, create_dt, update_dt )
select 
    account, 
    valid_to, 
    client, 
    create_dt, 
    update_dt from bank.accounts
where create_dt > ( 
    select 
        coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'accounts' ) or 
    update_dt>( 
    select 
        coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'accounts' )""");
 
 
 
time.sleep(2)
  
conn.commit()
   
curs.execute("""insert into demipt2.sape_stg_accounts_del ( account_num )
select 
    account 
from bank.accounts""");

time.sleep(2)
conn.commit()

curs.execute("""merge into demipt2.sape_dwh_dim_accounts_hist tgt
using (
    select s.account_num,
           s.update_dt
    from demipt2.sape_stg_accounts s
    left join demipt2.sape_dwh_dim_accounts_hist t
    on s.account_num = t.account_num and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
    where 
        t.account_num is not null and ( 1=0
  or t.VALID_TO <> s.VALID_TO or (t.VALID_TO is null and s.VALID_TO is not null) or ( t.VALID_TO is not null and s.VALID_TO is null)
 or t.CLIENT <> s.CLIENT or (t.CLIENT is null and s.CLIENT is not null) or ( t.CLIENT is not null and s.CLIENT is null))
    ) stg
on ( tgt.account_num = stg.account_num )
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )""")

time.sleep(2)
conn.commit()

curs.execute("""insert into demipt2.sape_dwh_dim_accounts_hist ( account_num, valid_to, client, effective_from, effective_to, deleted_flg )
select 
	account_num, 
	valid_to,
    client,
	coalesce (update_dt,create_dt) as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'N' as deleted_flg
from demipt2.sape_stg_accounts""");

time.sleep(2)
conn.commit()

#удаления

curs.execute("""insert into demipt2.sape_dwh_dim_accounts_hist ( account_num, valid_to, client, effective_from, effective_to, deleted_flg  )
select 
    account_num, 
	valid_to,
    client,
	to_date(?, 'dd.mm.YYYY hh24:mi:ss') as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'Y' as deleted_flg
from (
    select
        t.account_num, 
        valid_to,
        client
    from demipt2.sape_dwh_dim_accounts_hist t
    left join demipt2.sape_stg_accounts_del s
    on t.account_num = s.account_num 
    where s.account_num is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N' )""", (date_time,))

time.sleep(2)
conn.commit()

   
curs.execute ("""update demipt2.sape_dwh_dim_accounts_hist
set effective_to = to_date(?, 'dd.mm.YYYY hh24:mi:ss') - interval '1' second
where account_num in (
    select
       t.account_num
    from demipt2.sape_dwh_dim_accounts_hist t
    left join demipt2.sape_stg_accounts_del s
    on t.account_num = s.account_num  
    where s.account_num is null and deleted_flg='N' and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ))
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'""", (date_time,));

time.sleep(2)
conn.commit()

#Обновляем метаданные.
curs.execute("""update demipt2.sape_meta_trans
set last_update_dt = ( Coalesce((select max(update_dt) from demipt2.sape_stg_accounts), (select max(create_dt) from demipt2.sape_stg_accounts ) ))
where table_db = 'DEMIPT2' and table_name = 'accounts'""");

conn.commit()
time.sleep(1)

print('65%')

###Загружаем карты###

curs.execute("delete from demipt2.sape_stg_cards");

conn.commit()

curs.execute("delete from demipt2.sape_stg_cards_del");

conn.commit()

curs.execute("""insert into demipt2.sape_stg_cards ( card_num, account_num, create_dt, update_dt )
select 
    trim(card_num), 
    account, 
    create_dt, 
    update_dt 
from bank.cards
where create_dt > ( 
    select coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'cards' ) or update_dt>( 
    select coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'cards' )""");

time.sleep(2)  
conn.commit()

curs.execute ("""insert into demipt2.sape_stg_cards_del ( card_num )
select 
    trim(card_num) 
from bank.cards""");

time.sleep(1)
conn.commit()

curs.execute ("""merge into demipt2.sape_dwh_dim_cards_hist tgt
using (
    select s.card_num,
           s.update_dt
    from demipt2.sape_stg_cards s
    left join demipt2.sape_dwh_dim_cards_hist t
    on s.card_num = t.card_num and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
    where 
        t.card_num is not null and ( 1=0
  or t.account_num <> s.account_num or (t.account_num is null and s.account_num is not null) or ( t.account_num is not null and s.account_num is null))
    ) stg
on ( tgt.card_num = stg.card_num )
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )""")

time.sleep(1)
conn.commit()

curs.execute ("""insert into demipt2.sape_dwh_dim_cards_hist ( card_num, account_num, effective_from, effective_to, deleted_flg )
select 
	card_num, 
	account_num,
	coalesce (update_dt, create_dt) as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'N' as deleted_flg
from demipt2.sape_stg_cards""")

conn.commit()
time.sleep(1)
#Удаления
curs.execute ("""insert into demipt2.sape_dwh_dim_cards_hist (card_num, account_num, effective_from, effective_to, deleted_flg)
select 
    card_num, 
	account_num,
	to_date(?, 'dd.mm.YYYY hh24:mi:ss') as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'Y' as deleted_flg
from (
    select 
       t.card_num, 
       account_num
    from demipt2.sape_dwh_dim_cards_hist t
    left join demipt2.sape_stg_cards_del s
    on t.card_num = s.card_num 
    where s.card_num is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N' )""", (date_time,))
    
conn.commit()
time.sleep(1)	
curs.execute("""update demipt2.sape_dwh_dim_cards_hist
set effective_to = to_date(?, 'dd.mm.YYYY hh24:mi:ss') - interval '1' second
where card_num in (
    select
       t.card_num
    from demipt2.sape_dwh_dim_cards_hist t
    left join demipt2.sape_stg_cards_del s
    on t.card_num = s.card_num  
    where s.card_num is null and deleted_flg='N' and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ))
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'""", (date_time,))

conn.commit()
time.sleep(1)
#Обновляем метаданные.
curs.execute("""update demipt2.sape_meta_trans
set last_update_dt = ( Coalesce((select max(update_dt) from demipt2.sape_stg_cards), (select max(create_dt) from demipt2.sape_stg_cards ) ))
where table_db = 'DEMIPT2' and table_name = 'cards'""")

conn.commit()
time.sleep(1)

print('80%')

###Загружаем таблицу клиенты###

curs.execute("delete from demipt2.sape_stg_clients")

conn.commit()

curs.execute("delete from demipt2.sape_stg_clients_del")

conn.commit()

curs.execute("""insert into demipt2.sape_stg_clients ( client_id, last_name, first_name, patronymic, date_of_birth, passport_num, passport_valid_to, phone, create_dt, update_dt )
select 
    client_id, 
    last_name, 
    first_name, 
    patronymic, 
    date_of_birth, 
    passport_num, 
    passport_valid_to, 
    phone, 
    create_dt, 
    update_dt 
from bank.clients
where create_dt > ( 
    select coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'clients' ) or update_dt>( 
    select coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'clients' )""")
    
time.sleep(2)    
conn.commit()
    
curs.execute("""insert into demipt2.sape_stg_clients_del ( client_id )
select 
    client_id 
from bank.clients""")

conn.commit()
time.sleep(1)
curs.execute("""merge into demipt2.sape_dwh_dim_clients_hist tgt
using (
    select s.client_id,
           s.update_dt
    from demipt2.sape_stg_clients s
    left join demipt2.sape_dwh_dim_clients_hist t
    on s.client_id = t.client_id and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
    where 
        t.client_id is not null and ( 1=0
 or t.LAST_NAME <> s.LAST_NAME or (t.LAST_NAME is null and s.LAST_NAME is not null) or ( t.LAST_NAME is not null and s.LAST_NAME is null)
 or t.FIRST_NAME <> s.FIRST_NAME or (t.FIRST_NAME is null and s.FIRST_NAME is not null) or ( t.FIRST_NAME is not null and s.FIRST_NAME is null)
 or t.PATRONYMIC <> s.PATRONYMIC or (t.PATRONYMIC is null and s.PATRONYMIC is not null) or ( t.PATRONYMIC is not null and s.PATRONYMIC is null)
 or t.DATE_OF_BIRTH <> s.DATE_OF_BIRTH or (t.DATE_OF_BIRTH is null and s.DATE_OF_BIRTH is not null) or ( t.DATE_OF_BIRTH is not null and s.DATE_OF_BIRTH is null)
 or t.PASSPORT_NUM <> s.PASSPORT_NUM or (t.PASSPORT_NUM is null and s.PASSPORT_NUM is not null) or ( t.PASSPORT_NUM is not null and s.PASSPORT_NUM is null)
 or t.PASSPORT_VALID_TO <> s.PASSPORT_VALID_TO or (t.PASSPORT_VALID_TO is null and s.PASSPORT_VALID_TO is not null) or ( t.PASSPORT_VALID_TO is not null and s.PASSPORT_VALID_TO is null)
 or t.PHONE <> s.PHONE or (t.PHONE is null and s.PHONE is not null) or ( t.PHONE is not null and s.PHONE is null))
    ) stg
on ( tgt.client_id = stg.client_id )
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )""")

conn.commit()
time.sleep(1)
curs.execute("""insert into demipt2.sape_dwh_dim_clients_hist ( CLIENT_ID, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_OF_BIRTH, PASSPORT_VALID_TO, PASSPORT_NUM, PHONE, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG )
select 
	client_id, 
	last_name,
    first_name,
    patronymic,
    date_of_birth,
    passport_valid_to,
    passport_num,
    phone,
	coalesce (update_dt,create_dt) as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'N' as deleted_flg
from demipt2.sape_stg_clients""")

conn.commit()
time.sleep(1)

#Удаления

curs.execute("""insert into demipt2.sape_dwh_dim_clients_hist ( CLIENT_ID, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_OF_BIRTH, PASSPORT_VALID_TO, PASSPORT_NUM, PHONE, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG )
select 
    client_id, 
	last_name,
    first_name,
    patronymic,
    date_of_birth,
    passport_valid_to,
    passport_num,
    phone,
	to_date(?, 'dd.mm.YYYY hh24:mi:ss') as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'Y' as deleted_flg
from (
    select
      t.client_id, 
      t.last_name,
      t.first_name,
      t.patronymic,
      t.date_of_birth,
      t.passport_valid_to,
      t.passport_num,
      t.phone
    from demipt2.sape_dwh_dim_clients_hist t
    left join demipt2.sape_stg_clients_del s
    on t.client_id = s.client_id 
    where s.client_id is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N' )""", (date_time,))
	
conn.commit()	
time.sleep(1)
    
curs.execute("""update demipt2.sape_dwh_dim_clients_hist
set effective_to = to_date(?, 'dd.mm.YYYY hh24:mi:ss') - interval '1' second
where client_id in (
    select
       t.client_id
    from demipt2.sape_dwh_dim_clients_hist t
    left join demipt2.sape_stg_clients_del s
    on t.client_id = s.client_id  
    where s.client_id is null and deleted_flg='N' and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ))
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'""", (date_time,))

conn.commit()
time.sleep(1)

#Обновляем метаданные

curs.execute("""update demipt2.sape_meta_trans
set last_update_dt = ( Coalesce ( (select max(update_dt) from demipt2.sape_stg_clients), (select max(create_dt) from demipt2.sape_stg_clients )))
where table_db = 'DEMIPT2' and table_name = 'clients'""")

print('Данные загружены, идет формирование ежедневного отчета')

###Построение отчета###
conn.commit()
time.sleep(1)
curs.execute("""insert into sape_rep_fraud (event_dt, passport, fio, phone, event_type, report_dt) 
select  distinct
    trans_date, 
    passport_num, 
    fio, 
    phone, 
    prichina, 
    current_date
from (
    select 
        trans_id,
        case 
            when lead(terminal_city) over(partition by t.card_num order by trans_date)<>terminal_city  and (lead(trans_date) over(partition by t.card_num order by trans_date)-trans_date)<1 then lead(trans_date) over(partition by t.card_num order by trans_date)
            else trans_date
        end trans_date,    
        t.card_num, 
        c.account_num, 
        valid_to, 
        client, 
        oper_type, 
        amt, 
        oper_result, 
        terminal, 
        terminal_city, 
        last_name||' '||first_name||' '||patronymic as fio, 
        date_of_birth, 
        cl.passport_num, 
        phone,
        case
            when (coalesce(passport_valid_to, to_date('9999-12-31', 'YYYY-MM-DD' ))<trans_date and cl.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' ) and cl.deleted_flg='N') then '1'
            when (p.passport_num is not null and trunc(trans_date)=trunc(entry_dt) and version_dt=(select max(version_dt) from demipt2.sape_dwh_fact_pssprt_blcklst )) then '1'
            when (valid_to<trans_date and a.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' )and a.deleted_flg='N') then '2'
            when lead(terminal_city) over(partition by t.card_num order by trans_date)<>terminal_city  and (lead(trans_date) over(partition by t.card_num order by trans_date)-trans_date)<1 then '3'
            else 'OKEY'
        end prichina
    from demipt2.sape_dwh_fact_transactions  t
    left join demipt2.sape_dwh_dim_cards_hist c
    on t.card_num=c.card_num and c.effective_to=to_date( '9999-12-31', 'YYYY-MM-DD' )and c.deleted_flg='N'
    left join demipt2.sape_dwh_dim_accounts_hist a
    on c.account_num=a.account_num  and a.effective_to=to_date( '9999-12-31', 'YYYY-MM-DD' )and a.deleted_flg='N'
    left join demipt2.sape_dwh_dim_clients_hist cl
    on client=client_id and cl.effective_to=to_date( '9999-12-31', 'YYYY-MM-DD' )and cl.deleted_flg='N'
    left join demipt2.sape_dwh_fact_pssprt_blcklst p
    on cl.passport_num=p.passport_num
    left join demipt2.sape_dwh_dim_terminals_hist e
    on terminal=terminal_id and e.effective_to=to_date( '9999-12-31', 'YYYY-MM-DD' )and e.deleted_flg='N'
    where trans_date>=to_date ( '01.03.2021 23:00:00','DD.MM.YYYY HH24:MI:SS' ) and trans_date<to_date ( '03.03.2021 00:00:00','DD.MM.YYYY HH24:MI:SS' ))
where prichina<>'OKEY' and trans_date>=to_date ( '02.03.2021', 'dd.mm.yyyy' ) and trans_date<to_date ( '03.03.2021', 'dd.mm.yyyy' )
""")

conn.commit()



curs.execute("""insert into sape_rep_fraud (event_dt, passport, fio, phone, event_type, report_dt) 
select 
    min(trans_date), 
    passport_num, 
    fio, phone, 
    '4', 
    current_date  
from (
    select 
        trans_date, 
        passport_num, 
        last_name||' '||first_name||' '||patronymic as fio, 
        phone, 
        sum, 
        oper_result 
    from (
        select 
            trans_date, 
            passport_num, 
            last_name,
            first_name,
            patronymic, 
            oper_type, 
            phone, 
            oper_result,
            sum(q) over (partition by card_num order by trans_date range between interval '20' minute preceding and current row) sum
        from (
            select 
                trans_date, 
                t.card_num, 
                passport_num, 
                oper_type, 
                first_name, 
                last_name, 
                patronymic, 
                phone, 
                oper_result, 
                amt, 
                case
                    when oper_result='REJECT' and amt>lead(amt)over(partition by t.card_num order by trans_date) and oper_type<>'DEPOSIT'  then 1
                    else 0
                end q
            from sape_dwh_fact_transactions t
            left join demipt2.sape_dwh_dim_cards_hist c
            on t.card_num=c.card_num and c.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' )and c.deleted_flg='N'
            left join demipt2.sape_dwh_dim_accounts_hist a
            on c.account_num=a.account_num  and a.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' )and a.deleted_flg='N'
            left join demipt2.sape_dwh_dim_clients_hist cl
            on client=client_id and cl.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' )and cl.deleted_flg='N'))
where sum>=3 AND oper_result='SUCCESS' and trans_date>=to_date ('02.03.2021', 'dd.mm.yyyy') and trans_date<to_date ('03.03.2021', 'dd.mm.yyyy'))
group by passport_num, fio, phone, current_date""")

conn.commit()
print('Отчет подготовлен')
time.sleep(1)
conn.commit()
print('старт загрузки третьего дня')

###Загружаем первый день Blacklist###

#Очищаем стэйджинг таблицы терминалов
curs.execute("delete from demipt2.sape_stg_passport_blacklist")
#Зачитываем файл
df = pandas.read_excel( '/home/demipt2/sape/passport_blacklist_03032021.xlsx', sheet_name='blacklist',header=0, decimal=',', index_col=None )
df['date']=df['date'].astype(str)
#Отправляем данные в базу
curs.executemany( "insert into demipt2.sape_stg_passport_blacklist (ENTRY_DT, PASSPORT_NUM) values (to_date(?, 'YYYY-MM-DD'), ?)",
df.values.tolist() )

conn.commit()
print('15%')


#Делаем простую вставку изменений
curs.execute("""insert into demipt2.sape_dwh_fact_pssprt_blcklst (PASSPORT_NUM, ENTRY_DT, 	VERSION_DT )
select PASSPORT_NUM, ENTRY_DT, (select max(entry_dt) from demipt2.sape_stg_passport_blacklist) from demipt2.sape_stg_passport_blacklist""");


#Фиксируем транзакцию
conn.commit();

os.rename("/home/demipt2/sape/passport_blacklist_03032021.xlsx", "/home/demipt2/sape/archive/passport_blacklist_03032021.xlsx.backup")
###Загружаем транзакции####

#Очищаем стэйджинг таблицы терминалов
curs.execute("delete from demipt2.sape_stg_transactions")
#Зачитываем файл
df = pandas.read_csv( '/home/demipt2/sape/transactions_03032021.txt', sep=';', header=0, decimal=',', index_col=None )
df['transaction_date']=df['transaction_date'].astype(str)
#Отправляем данные в базу
curs.executemany( "insert into demipt2.sape_stg_transactions (TRANS_ID, TRANS_DATE, AMT, CARD_NUM, OPER_TYPE, OPER_RESULT, TERMINAL) values (?, to_date(?, 'yyyy-mm-dd hh24:mi:ss'), ?, ?, ?, ?, ?)",
df.values.tolist() )



curs.execute("""insert into demipt2.sape_dwh_fact_transactions (TRANS_ID, TRANS_DATE, CARD_NUM, OPER_TYPE, AMT, OPER_RESULT, TERMINAL )
select 
    TRANS_ID,
    TRANS_DATE,
    CARD_NUM,
    OPER_TYPE,
    AMT,
    OPER_RESULT,
    TERMINAL 
from demipt2.sape_stg_transactions""");

conn.commit()

os.rename("/home/demipt2/sape/transactions_03032021.txt", "/home/demipt2/sape/archive/transactions_03032021.txt.backup")
print('30%')


###Загружаем первый день terminals###

#Очищаем стэйджинг таблицы терминалов
curs.execute("delete from demipt2.sape_stg_term_part1")
curs.execute("delete from demipt2.sape_stg_terminals_del")
#Зачитываем файл
df = pandas.read_excel( '/home/demipt2/sape/terminals_03032021.xlsx', sheet_name='terminals',header=0, decimal=',', index_col=None )

#Отправляем данные в базу
curs.executemany( "insert into demipt2.sape_stg_term_part1 (terminal_id, terminal_type, terminal_city, terminal_address,download_dt) values (?,?,?,?, to_date( '2021-03-03', 'YYYY-MM-DD' ))",
df.values.tolist() )

conn.commit()

curs.execute("""insert into demipt2.sape_stg_terminals_del ( terminal_id )
select 
    terminal_id 
from demipt2.sape_stg_term_part1""")
conn.commit()

time.sleep(1)

curs.execute("""merge into demipt2.sape_dwh_dim_terminals_hist tgt
using (
    select 
        s.terminal_id, 
        download_dt
    from demipt2.sape_stg_term_part1 s
    left join demipt2.sape_dwh_dim_terminals_hist t
    on s.terminal_id = t.terminal_id and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
    where 
        t.terminal_id is not null and ( 1=0
 or t.TERMINAL_TYPE <> s.TERMINAL_TYPE or (t.TERMINAL_TYPE is null and s.TERMINAL_TYPE is not null) or ( t.TERMINAL_TYPE is not null and s.TERMINAL_TYPE is null)
 or t.TERMINAL_CITY <> s.TERMINAL_CITY or (t.TERMINAL_CITY is null and s.TERMINAL_CITY is not null) or ( t.TERMINAL_CITY is not null and s.TERMINAL_CITY is null)
 or t.TERMINAL_ADDRESS <> s.TERMINAL_ADDRESS or (t.TERMINAL_ADDRESS is null and s.TERMINAL_ADDRESS is not null) or ( t.TERMINAL_ADDRESS is not null and s.TERMINAL_ADDRESS is null))
    ) stg
on ( tgt.terminal_id = stg.terminal_id )
when matched then update set effective_to = download_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )""");

conn.commit()
time.sleep(1)

curs.execute("""insert into demipt2.sape_dwh_dim_terminals_hist ( terminal_id, terminal_type, terminal_city, terminal_address, effective_from, effective_to, deleted_flg )
select 
    t.terminal_id, 
    t. terminal_type, 
    t.terminal_city, 
    t.terminal_address,
    download_dt as effective_from,
    to_date( '9999-12-31', 'YYYY-MM-DD') as effective_to, 
    'N' as deleted_flg
from demipt2.sape_stg_term_part1 t 
left join demipt2.sape_dwh_dim_terminals_hist s on t.terminal_id=s.terminal_id and effective_to=to_date( '9999-12-31', 'YYYY-MM-DD')
where s.terminal_id is null or ( 1=0
 or t.TERMINAL_TYPE <> s.TERMINAL_TYPE or (t.TERMINAL_TYPE is null and s.TERMINAL_TYPE is not null) or ( t.TERMINAL_TYPE is not null and s.TERMINAL_TYPE is null)
 or t.TERMINAL_CITY <> s.TERMINAL_CITY or (t.TERMINAL_CITY is null and s.TERMINAL_CITY is not null) or ( t.TERMINAL_CITY is not null and s.TERMINAL_CITY is null)
 or t.TERMINAL_ADDRESS <> s.TERMINAL_ADDRESS or (t.TERMINAL_ADDRESS is null and s.TERMINAL_ADDRESS is not null) or ( t.TERMINAL_ADDRESS is not null and s.TERMINAL_ADDRESS is null))""")
         
conn.commit()


#удаления
curs.execute("""insert into demipt2.sape_dwh_dim_terminals_hist ( terminal_id, terminal_type, terminal_city, terminal_address, effective_from, effective_to, deleted_flg  )
select 
    terminal_id, 
	terminal_type, 
    terminal_city, 
    terminal_address,
	to_date( '2021-03-03', 'YYYY-MM-DD' ) as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'Y' as deleted_flg
from (
    select 
       t.terminal_id, 
        terminal_type, 
        terminal_city, 
        terminal_address
    from demipt2.sape_dwh_dim_terminals_hist t
    left join demipt2.sape_stg_terminals_del s
    on t.terminal_id = s.terminal_id 
    where s.terminal_id is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N' )""")
 
          
conn.commit()
    
curs.execute("""update demipt2.sape_dwh_dim_terminals_hist
set effective_to = to_date( '2021-03-03', 'YYYY-MM-DD' ) - interval '1' second
where terminal_id in (
    select
       t.terminal_id
    from demipt2.sape_dwh_dim_terminals_hist t
    left join demipt2.sape_stg_terminals_del s
    on t.terminal_id = s.terminal_id  
    where s.terminal_id is null and deleted_flg='N' and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ))
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'""");

conn.commit()
os.rename("/home/demipt2/sape/terminals_03032021.xlsx", "/home/demipt2/sape/archive/terminals_03032021.xlsx.backup")

print('45%')

###Загружаем изменения по таблице аккаунты###


curs.execute("delete from demipt2.sape_stg_accounts");

conn.commit()

curs.execute("delete from demipt2.sape_stg_accounts_del");

time.sleep(2)
conn.commit()

curs.execute("""insert into demipt2.sape_stg_accounts ( account_num, valid_to, client, create_dt, update_dt )
select 
    account, 
    valid_to, 
    client, 
    create_dt, 
    update_dt from bank.accounts
where create_dt > ( 
    select 
        coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'accounts' ) or 
    update_dt>( 
    select 
        coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'accounts' )""");
 
 
 
time.sleep(2)
  
conn.commit()
   
curs.execute("""insert into demipt2.sape_stg_accounts_del ( account_num )
select 
    account 
from bank.accounts""");

time.sleep(2)
conn.commit()

curs.execute("""merge into demipt2.sape_dwh_dim_accounts_hist tgt
using (
    select s.account_num,
           s.update_dt
    from demipt2.sape_stg_accounts s
    left join demipt2.sape_dwh_dim_accounts_hist t
    on s.account_num = t.account_num and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
    where 
        t.account_num is not null and ( 1=0
  or t.VALID_TO <> s.VALID_TO or (t.VALID_TO is null and s.VALID_TO is not null) or ( t.VALID_TO is not null and s.VALID_TO is null)
 or t.CLIENT <> s.CLIENT or (t.CLIENT is null and s.CLIENT is not null) or ( t.CLIENT is not null and s.CLIENT is null))
    ) stg
on ( tgt.account_num = stg.account_num )
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )""")

time.sleep(2)
conn.commit()

curs.execute("""insert into demipt2.sape_dwh_dim_accounts_hist ( account_num, valid_to, client, effective_from, effective_to, deleted_flg )
select 
	account_num, 
	valid_to,
    client,
	coalesce (update_dt,create_dt) as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'N' as deleted_flg
from demipt2.sape_stg_accounts""");

time.sleep(2)
conn.commit()

#удаления

curs.execute("""insert into demipt2.sape_dwh_dim_accounts_hist ( account_num, valid_to, client, effective_from, effective_to, deleted_flg  )
select 
    account_num, 
	valid_to,
    client,
	to_date(?, 'dd.mm.YYYY hh24:mi:ss') as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'Y' as deleted_flg
from (
    select
        t.account_num, 
        valid_to,
        client
    from demipt2.sape_dwh_dim_accounts_hist t
    left join demipt2.sape_stg_accounts_del s
    on t.account_num = s.account_num 
    where s.account_num is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N' )""", (date_time,))

time.sleep(2)
conn.commit()

   
curs.execute ("""update demipt2.sape_dwh_dim_accounts_hist
set effective_to = to_date(?, 'dd.mm.YYYY hh24:mi:ss') - interval '1' second
where account_num in (
    select
       t.account_num
    from demipt2.sape_dwh_dim_accounts_hist t
    left join demipt2.sape_stg_accounts_del s
    on t.account_num = s.account_num  
    where s.account_num is null and deleted_flg='N' and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ))
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'""", (date_time,));

time.sleep(2)
conn.commit()

#Обновляем метаданные.
curs.execute("""update demipt2.sape_meta_trans
set last_update_dt = ( Coalesce((select max(update_dt) from demipt2.sape_stg_accounts), (select max(create_dt) from demipt2.sape_stg_accounts ) ))
where table_db = 'DEMIPT2' and table_name = 'accounts'""");

conn.commit()
time.sleep(1)

print('70%')

###Загружаем карты###

curs.execute("delete from demipt2.sape_stg_cards");

conn.commit()

curs.execute("delete from demipt2.sape_stg_cards_del");

conn.commit()

curs.execute("""insert into demipt2.sape_stg_cards ( card_num, account_num, create_dt, update_dt )
select 
    trim(card_num), 
    account, 
    create_dt, 
    update_dt 
from bank.cards
where create_dt > ( 
    select coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'cards' ) or update_dt>( 
    select coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'cards' )""");

time.sleep(2)  
conn.commit()

curs.execute ("""insert into demipt2.sape_stg_cards_del ( card_num )
select 
    trim(card_num) 
from bank.cards""");

time.sleep(1)
conn.commit()

curs.execute ("""merge into demipt2.sape_dwh_dim_cards_hist tgt
using (
    select s.card_num,
           s.update_dt
    from demipt2.sape_stg_cards s
    left join demipt2.sape_dwh_dim_cards_hist t
    on s.card_num = t.card_num and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
    where 
        t.card_num is not null and ( 1=0
  or t.account_num <> s.account_num or (t.account_num is null and s.account_num is not null) or ( t.account_num is not null and s.account_num is null))
    ) stg
on ( tgt.card_num = stg.card_num )
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )""")

time.sleep(1)
conn.commit()

curs.execute ("""insert into demipt2.sape_dwh_dim_cards_hist ( card_num, account_num, effective_from, effective_to, deleted_flg )
select 
	card_num, 
	account_num,
	coalesce (update_dt, create_dt) as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'N' as deleted_flg
from demipt2.sape_stg_cards""")

conn.commit()
time.sleep(1)
#Удаления
curs.execute ("""insert into demipt2.sape_dwh_dim_cards_hist (card_num, account_num, effective_from, effective_to, deleted_flg)
select 
    card_num, 
	account_num,
	to_date(?, 'dd.mm.YYYY hh24:mi:ss') as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'Y' as deleted_flg
from (
    select 
       t.card_num, 
       account_num
    from demipt2.sape_dwh_dim_cards_hist t
    left join demipt2.sape_stg_cards_del s
    on t.card_num = s.card_num 
    where s.card_num is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N' )""", (date_time,))
    
conn.commit()
time.sleep(1)	
curs.execute("""update demipt2.sape_dwh_dim_cards_hist
set effective_to = to_date(?, 'dd.mm.YYYY hh24:mi:ss') - interval '1' second
where card_num in (
    select
       t.card_num
    from demipt2.sape_dwh_dim_cards_hist t
    left join demipt2.sape_stg_cards_del s
    on t.card_num = s.card_num  
    where s.card_num is null and deleted_flg='N' and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ))
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'""", (date_time,))

conn.commit()
time.sleep(1)
#Обновляем метаданные.
curs.execute("""update demipt2.sape_meta_trans
set last_update_dt = ( Coalesce((select max(update_dt) from demipt2.sape_stg_cards), (select max(create_dt) from demipt2.sape_stg_cards ) ))
where table_db = 'DEMIPT2' and table_name = 'cards'""")

conn.commit()
time.sleep(1)

print('90%')

###Загружаем таблицу клиенты###

curs.execute("delete from demipt2.sape_stg_clients")

conn.commit()

curs.execute("delete from demipt2.sape_stg_clients_del")

conn.commit()

curs.execute("""insert into demipt2.sape_stg_clients ( client_id, last_name, first_name, patronymic, date_of_birth, passport_num, passport_valid_to, phone, create_dt, update_dt )
select 
    client_id, 
    last_name, 
    first_name, 
    patronymic, 
    date_of_birth, 
    passport_num, 
    passport_valid_to, 
    phone, 
    create_dt, 
    update_dt 
from bank.clients
where create_dt > ( 
    select coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'clients' ) or update_dt>( 
    select coalesce( last_update_dt, to_date( '1899-01-01', 'YYYY-MM-DD') ) 
    from demipt2.sape_meta_trans where table_db = 'DEMIPT2' and table_name = 'clients' )""")
    
time.sleep(2)    
conn.commit()
    
curs.execute("""insert into demipt2.sape_stg_clients_del ( client_id )
select 
    client_id 
from bank.clients""")

conn.commit()
time.sleep(1)
curs.execute("""merge into demipt2.sape_dwh_dim_clients_hist tgt
using (
    select s.client_id,
           s.update_dt
    from demipt2.sape_stg_clients s
    left join demipt2.sape_dwh_dim_clients_hist t
    on s.client_id = t.client_id and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N'
    where 
        t.client_id is not null and ( 1=0
 or t.LAST_NAME <> s.LAST_NAME or (t.LAST_NAME is null and s.LAST_NAME is not null) or ( t.LAST_NAME is not null and s.LAST_NAME is null)
 or t.FIRST_NAME <> s.FIRST_NAME or (t.FIRST_NAME is null and s.FIRST_NAME is not null) or ( t.FIRST_NAME is not null and s.FIRST_NAME is null)
 or t.PATRONYMIC <> s.PATRONYMIC or (t.PATRONYMIC is null and s.PATRONYMIC is not null) or ( t.PATRONYMIC is not null and s.PATRONYMIC is null)
 or t.DATE_OF_BIRTH <> s.DATE_OF_BIRTH or (t.DATE_OF_BIRTH is null and s.DATE_OF_BIRTH is not null) or ( t.DATE_OF_BIRTH is not null and s.DATE_OF_BIRTH is null)
 or t.PASSPORT_NUM <> s.PASSPORT_NUM or (t.PASSPORT_NUM is null and s.PASSPORT_NUM is not null) or ( t.PASSPORT_NUM is not null and s.PASSPORT_NUM is null)
 or t.PASSPORT_VALID_TO <> s.PASSPORT_VALID_TO or (t.PASSPORT_VALID_TO is null and s.PASSPORT_VALID_TO is not null) or ( t.PASSPORT_VALID_TO is not null and s.PASSPORT_VALID_TO is null)
 or t.PHONE <> s.PHONE or (t.PHONE is null and s.PHONE is not null) or ( t.PHONE is not null and s.PHONE is null))
    ) stg
on ( tgt.client_id = stg.client_id )
when matched then update set effective_to = stg.update_dt - interval '1' second where effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )""")

conn.commit()
time.sleep(1)
curs.execute("""insert into demipt2.sape_dwh_dim_clients_hist ( CLIENT_ID, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_OF_BIRTH, PASSPORT_VALID_TO, PASSPORT_NUM, PHONE, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG )
select 
	client_id, 
	last_name,
    first_name,
    patronymic,
    date_of_birth,
    passport_valid_to,
    passport_num,
    phone,
	coalesce (update_dt,create_dt) as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'N' as deleted_flg
from demipt2.sape_stg_clients""")

conn.commit()
time.sleep(1)

#Удаления

curs.execute("""insert into demipt2.sape_dwh_dim_clients_hist ( CLIENT_ID, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_OF_BIRTH, PASSPORT_VALID_TO, PASSPORT_NUM, PHONE, EFFECTIVE_FROM, EFFECTIVE_TO, DELETED_FLG )
select 
    client_id, 
	last_name,
    first_name,
    patronymic,
    date_of_birth,
    passport_valid_to,
    passport_num,
    phone,
	to_date(?, 'dd.mm.YYYY hh24:mi:ss') as effective_from, 
	to_date( '9999-12-31', 'YYYY-MM-DD' ) as effective_to, 
	'Y' as deleted_flg
from (
    select
      t.client_id, 
      t.last_name,
      t.first_name,
      t.patronymic,
      t.date_of_birth,
      t.passport_valid_to,
      t.passport_num,
      t.phone
    from demipt2.sape_dwh_dim_clients_hist t
    left join demipt2.sape_stg_clients_del s
    on t.client_id = s.client_id 
    where s.client_id is null and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ) and deleted_flg = 'N' )""", (date_time,))
	
conn.commit()	
time.sleep(1)
    
curs.execute("""update demipt2.sape_dwh_dim_clients_hist
set effective_to = to_date(?, 'dd.mm.YYYY hh24:mi:ss') - interval '1' second
where client_id in (
    select
       t.client_id
    from demipt2.sape_dwh_dim_clients_hist t
    left join demipt2.sape_stg_clients_del s
    on t.client_id = s.client_id  
    where s.client_id is null and deleted_flg='N' and t.effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' ))
and effective_to = to_date( '9999-12-31', 'YYYY-MM-DD' )
and deleted_flg = 'N'""", (date_time,))

conn.commit()
time.sleep(1)

#Обновляем метаданные

curs.execute("""update demipt2.sape_meta_trans
set last_update_dt = ( Coalesce ( (select max(update_dt) from demipt2.sape_stg_clients), (select max(create_dt) from demipt2.sape_stg_clients )))
where table_db = 'DEMIPT2' and table_name = 'clients'""")

print('Данные загружены, идет формирование ежедневного отчета')

###Построение отчета###
conn.commit()
time.sleep(1)
curs.execute("""insert into sape_rep_fraud (event_dt, passport, fio, phone, event_type, report_dt) 
select distinct 
    trans_date, 
    passport_num, 
    fio, 
    phone, 
    prichina, 
    current_date
from (
    select 
        trans_id,
        case 
            when lead(terminal_city) over(partition by t.card_num order by trans_date)<>terminal_city  and (lead(trans_date) over(partition by t.card_num order by trans_date)-trans_date)<1 then lead(trans_date) over(partition by t.card_num order by trans_date)
            else trans_date
        end trans_date,    
        t.card_num, 
        c.account_num, 
        valid_to, 
        client, 
        oper_type, 
        amt, 
        oper_result, 
        terminal, 
        terminal_city, 
        last_name||' '||first_name||' '||patronymic as fio, 
        date_of_birth, 
        cl.passport_num, 
        phone,
        case
            when (coalesce(passport_valid_to, to_date('9999-12-31', 'YYYY-MM-DD' ))<trans_date and cl.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' ) and cl.deleted_flg='N') then '1'
            when (p.passport_num is not null and trunc(trans_date)=trunc(entry_dt) and version_dt=(select max(version_dt) from demipt2.sape_dwh_fact_pssprt_blcklst )) then '1'
            when (valid_to<trans_date and a.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' )and a.deleted_flg='N') then '2'
            when lead(terminal_city) over(partition by t.card_num order by trans_date)<>terminal_city  and (lead(trans_date) over(partition by t.card_num order by trans_date)-trans_date)<1 then '3'
            else 'OKEY'
        end prichina
    from demipt2.sape_dwh_fact_transactions  t
    left join demipt2.sape_dwh_dim_cards_hist c
    on t.card_num=c.card_num and c.effective_to=to_date( '9999-12-31', 'YYYY-MM-DD' )and c.deleted_flg='N'
    left join demipt2.sape_dwh_dim_accounts_hist a
    on c.account_num=a.account_num  and a.effective_to=to_date( '9999-12-31', 'YYYY-MM-DD' )and a.deleted_flg='N'
    left join demipt2.sape_dwh_dim_clients_hist cl
    on client=client_id and cl.effective_to=to_date( '9999-12-31', 'YYYY-MM-DD' )and cl.deleted_flg='N'
    left join demipt2.sape_dwh_fact_pssprt_blcklst p
    on cl.passport_num=p.passport_num
    left join demipt2.sape_dwh_dim_terminals_hist e
    on terminal=terminal_id and e.effective_to=to_date( '9999-12-31', 'YYYY-MM-DD' )and e.deleted_flg='N'
    where trans_date>=to_date ( '02.03.2021 23:00:00','DD.MM.YYYY HH24:MI:SS' ) and trans_date<to_date ( '04.03.2021 00:00:00','DD.MM.YYYY HH24:MI:SS' ))
where prichina<>'OKEY' and trans_date>=to_date ( '03.03.2021', 'dd.mm.yyyy' ) and trans_date<to_date ( '04.03.2021', 'dd.mm.yyyy' )""")

conn.commit()


curs.execute("""insert into sape_rep_fraud (event_dt, passport, fio, phone, event_type, report_dt) 
select 
    min(trans_date), 
    passport_num, 
    fio, phone, 
    '4', 
    current_date  
from (
    select 
        trans_date, 
        passport_num, 
        last_name||' '||first_name||' '||patronymic as fio, 
        phone, 
        sum, 
        oper_result 
    from (
        select 
            trans_date, 
            passport_num, 
            last_name,
            first_name,
            patronymic, 
            oper_type, 
            phone, 
            oper_result,
            sum(q) over (partition by card_num order by trans_date range between interval '20' minute preceding and current row) sum
        from (
            select 
                trans_date, 
                t.card_num, 
                passport_num, 
                oper_type, 
                first_name, 
                last_name, 
                patronymic, 
                phone, 
                oper_result, 
                amt, 
                case
                    when oper_result='REJECT' and amt>lead(amt)over(partition by t.card_num order by trans_date) and oper_type<>'DEPOSIT'  then 1
                    else 0
                end q
            from sape_dwh_fact_transactions t
            left join demipt2.sape_dwh_dim_cards_hist c
            on t.card_num=c.card_num and c.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' )and c.deleted_flg='N'
            left join demipt2.sape_dwh_dim_accounts_hist a
            on c.account_num=a.account_num  and a.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' )and a.deleted_flg='N'
            left join demipt2.sape_dwh_dim_clients_hist cl
            on client=client_id and cl.effective_to=to_date('9999-12-31', 'YYYY-MM-DD' )and cl.deleted_flg='N'))
where sum>=3 AND oper_result='SUCCESS' and trans_date>=to_date ('03.03.2021', 'dd.mm.yyyy') and trans_date<to_date ('04.03.2021', 'dd.mm.yyyy'))
group by passport_num, fio, phone, current_date""")

conn.commit()
print('Отчет подготовлен')
time.sleep(1)
conn.commit()
curs.close()
conn.close()
